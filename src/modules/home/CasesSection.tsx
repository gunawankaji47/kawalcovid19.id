import * as React from 'react';
import useSWR from 'swr/dist';
import styled from '@emotion/styled';
import format from 'date-fns/format';
import { id } from 'date-fns/locale';

import { Box, Heading, themeProps, Text, Stack } from 'components/design-system';
import { fetch } from 'utils/api';
import { logEventClick } from 'utils/analytics';
import SummaryApiResponse, { SUMMARY_API_URL } from 'types/api';

const EMPTY_DASH = '----';
const ERROR_TITLE = 'Error - Gagal mengambil data terbaru';
const ERROR_MESSAGE = 'Gagal mengambil data. Mohon coba lagi dalam beberapa saat.';

const Link = Text.withComponent('a');

const ReadmoreLink = styled(Link)`
  text-decoration: none;

  &:hover,
  &:focus {
    text-decoration: underline;
  }
`;

const GridWrapper = styled(Box)`
  display: grid;
  grid-template-columns: repeat(auto-fill, 1fr);
  grid-gap: 24px;

  ${themeProps.mediaQueries.md} {
    grid-template-columns: repeat(
      auto-fill,
      minmax(calc(${themeProps.widths.md}px / 2 - 48px), 1fr)
    );
  }

  ${themeProps.mediaQueries.lg} {
    grid-template-columns: repeat(
      auto-fill,
      minmax(calc(${themeProps.widths.lg}px / 2 - 48px), 1fr)
    );
  }

  ${themeProps.mediaQueries.xl} {
    grid-template-columns: repeat(
      auto-fill,
      minmax(calc(${themeProps.widths.xl}px / 4 - 48px), 1fr)
    );
  }
`;

const formatDate = (dateToFormat: Date) => {
  return format(dateToFormat, 'dd MMMM yyyy HH:mm:ss xxxxx', {
    locale: id,
  });
};

export interface CaseBoxProps {
  color: string;
  value?: number;
  label: string;
}

const CaseBox: React.FC<CaseBoxProps> = React.memo(({ color, value, label }) => (
  <Box
    display="flex"
    alignItems="center"
    justifyContent="center"
    px="md"
    pt="md"
    pb="lg"
    borderRadius={6}
    backgroundColor="accents01"
  >
    <Box textAlign="center">
      <Text display="block" variant={1100} color={color} fontFamily="monospace">
        {value || EMPTY_DASH}
      </Text>
      <Text display="block" mt="xxs" variant={500} color="accents08">
        {label}
      </Text>
    </Box>
  </Box>
));

export interface CasesSectionBlockProps {
  data?: SummaryApiResponse;
  error: boolean;
}

const CasesSectionBlock: React.FC<CasesSectionBlockProps> = React.memo(({ data, error }) => {
  const lastUpdatedAt = data?.metadata?.lastUpdatedAt;
  return (
    <Stack spacing="xl" mb="xxl">
      <Heading variant={800} as="h2">
        Jumlah Kasus di Indonesia Saat Ini
      </Heading>
      <Box>
        <GridWrapper>
          <CaseBox color="chart" value={data?.confirmed} label="Terkonfirmasi" />
          <CaseBox color="warning02" value={data?.activeCare} label="Dalam Perawatan" />
          <CaseBox color="success02" value={data?.recovered} label="Sembuh" />
          <CaseBox color="error02" value={data?.deceased} label="Meninggal" />
        </GridWrapper>
        <Box my="md">
          <Text as="h5" m={0} variant={200} color="accents06" fontWeight={400}>
            {error ? ERROR_TITLE : 'Pembaruan Terakhir'}
          </Text>
          <Text as="p" variant={400} color="accents07" fontFamily="monospace">
            {lastUpdatedAt ? formatDate(new Date(lastUpdatedAt)) : ERROR_MESSAGE}
          </Text>
        </Box>
        <ReadmoreLink
          href="http://kcov.id/statistik-harian"
          target="_blank"
          rel="noopener noreferrer"
          color="primary02"
          variant={500}
          onClick={() => logEventClick('Statistik Harian (cases section)')}
        >
          Lihat Statistik Harian &rarr;
        </ReadmoreLink>
      </Box>
    </Stack>
  );
});

const CasesSection: React.FC = () => {
  const { data, error } = useSWR<SummaryApiResponse | undefined>(SUMMARY_API_URL, fetch);
  return <CasesSectionBlock data={data} error={!!error} />;
};

export default CasesSection;
