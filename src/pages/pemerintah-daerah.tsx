import * as React from 'react';
import { NextPage } from 'next';
import { PageWrapper, Content, Column } from 'components/layout';
import { logEventClick } from 'utils/analytics';
import { SectionHeader } from 'modules/posts-index';
import { Box, Heading, UnstyledAnchor, themeProps } from 'components/design-system';
import styled from '@emotion/styled';

import pemdaLinks from 'content/pemdaLinks.json';

const Section = Content.withComponent('section');

export interface PemerintahDaerahDetails {
  name: string;
  link: string;
}

interface PemerintahDaerahContentPageProps {
  pemdaList?: PemerintahDaerahDetails;
  errors?: string;
}

const GridWrapper = styled(Box)`
  display: grid;
  grid-template-columns: repeat(auto-fill, 1fr);
  grid-gap: 24px;

  ${themeProps.mediaQueries.md} {
    grid-template-columns: repeat(
      auto-fill,
      minmax(calc(${themeProps.widths.xl}px / 3 - 48px), 1fr)
    );
  }
`;

const PemerintahDaerahListGrid: React.FC = ({ children }) => {
  return <GridWrapper>{children}</GridWrapper>;
};

const PemerintahDaerahLink = styled(UnstyledAnchor)`
  text-decoration: none;

  &:hover,
  &:focus {
    text-decoration: underline;
  }
`;

const PemerintahDaerahCard: React.FC<PemerintahDaerahDetails> = ({ name, link }) => {
  return (
    <Box as="article" display="flex" flexDirection="column" bg="accents01" borderRadius={8}>
      <PemerintahDaerahLink
        href={link}
        target="_blank"
        rel="noopener noreferrer"
        onClick={() => logEventClick(`[Pemda] ${name}`)}
      >
        <Box p="md">
          <Heading variant={600} as="h3">
            {name}
          </Heading>
          <Heading variant={100} as="h6">
            {link}
          </Heading>
        </Box>
      </PemerintahDaerahLink>
    </Box>
  );
};

const PemerintahDaerahPage: NextPage<PemerintahDaerahContentPageProps> = () => {
  return (
    <PageWrapper
      title={`Daftar Situs Pemerintah Daerah | KawalCOVID19`}
      description={
        'Halaman ini berisi daftar situs resmi pemerintah daerah yang berkaitan dengan penanganan COVID-19 di Indonesia'
      }
      pageTitle="Daftar Situs Pemerintah Daerah"
    >
      <SectionHeader
        title={'Situs Pemerintah Daerah'}
        description={
          'Berikut merupakan daftar situs resmi pemerintah daerah dalam hal penanganan COVID-19'
        }
      />
      <Section>
        <Column>
          <Box mb="xxl">
            <Heading variant={700} mb="xl" as="h2">
              Provinsi
            </Heading>
            <PemerintahDaerahListGrid>
              {pemdaLinks.provinsi.map(pemda => (
                <PemerintahDaerahCard key={pemda.name} name={pemda.name} link={pemda.link} />
              ))}
            </PemerintahDaerahListGrid>
          </Box>
          <Box mb="xxl">
            <Heading variant={700} mb="xl" as="h2">
              Kabupaten/Kota
            </Heading>
            <PemerintahDaerahListGrid>
              {pemdaLinks.kabupaten_kota.map(pemda => (
                <PemerintahDaerahCard key={pemda.name} name={pemda.name} link={pemda.link} />
              ))}
            </PemerintahDaerahListGrid>
          </Box>
        </Column>
      </Section>
    </PageWrapper>
  );
};

export default PemerintahDaerahPage;
