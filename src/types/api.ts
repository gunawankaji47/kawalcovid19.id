export const SUMMARY_API_URL = 'https://api.kawalcovid19.id/case/summary';

export interface Summary {
  confirmed: number;
  activeCare: number;
  recovered: number;
  deceased: number;
}

export default interface SummaryApiResponse extends Summary {
  metadata: {
    lastUpdatedAt: string;
  };
}
